# WebRTC

Web Real-Time Communication

* [Can I use _?
  ](https://caniuse.com/?search=WebRTC)

# Official documentation
* [webrtc.org](https://webrtc.org)
* [*WebRTC 1.0: Real-Time Communication Between Browsers*
  ](https://www.w3.org/TR/webrtc/)
  W3C

# Unofficial documentation
* [*WebRTC*
  ](https://en.m.wikipedia.org/wiki/WebRTC)